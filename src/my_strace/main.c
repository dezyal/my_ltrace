#include "my_strace.h"

int 
main(int argc, char **argv, char **envp) 
{
  if (argc == 1)
    return 1;
  trace_process(argv, envp);
  return 0;
}
