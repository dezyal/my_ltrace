#define _GNU_SOURCE
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/user.h>
#include <sys/reg.h>
#include <errno.h>

#include "my_strace.h"
#include "display_syscall.h"

static void
run_process(char **argv, char **envp);

static void
trace_pid(pid_t pid);

void
trace_process(char **argv, char **envp)
{
  pid_t pid = fork();
  if (!pid)
    run_process(argv, envp);
  else
    trace_pid(pid);
}

static void
run_process(char **argv, char **envp)
{
  ptrace(PTRACE_TRACEME, 0, NULL, NULL);
  if (execvpe(argv[1], argv + 1, envp) == -1)
    perror("error");
}

static void
trace_pid(pid_t pid)
{
  long rax;
  int status;
  int in_syscall = 0;
  struct user_regs_struct regs;

  wait(&status);
  while (1)
  {
    ptrace(PTRACE_SYSCALL, pid, 0, 0);
    wait(&status);
    if (WIFEXITED(status))
      break;

    if (!in_syscall)
    {
      ptrace(PTRACE_GETREGS, pid, NULL, &regs);
      display_syscall(regs, pid);
      in_syscall = 1;
    }
    else
    {
      rax = ptrace(PTRACE_PEEKUSER, pid, sizeof (long) * RAX, NULL);

      if (regs.orig_rax != 231)
        fprintf(stderr, ") = %ld\n", rax);
      in_syscall = 0;
    }
  }
}
