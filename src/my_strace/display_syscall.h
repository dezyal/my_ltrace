#ifndef DISPLAY_SYSCALL_H
# define DISPLAY_SYSCALL_H

void
display_syscall(struct user_regs_struct regs, pid_t pid);

#endif /* !DISPLAY_SYSCALL_H */
