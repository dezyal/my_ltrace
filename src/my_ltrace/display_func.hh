#ifndef DISPLAY_FUNC_HH
# define DISPLAY_FUNC_HH

size_t
display_function_call(pid_t pid, struct user_regs_struct  regs, s_elf_info *e);

#endif /* !DISPLAY_FUNC_HH */
