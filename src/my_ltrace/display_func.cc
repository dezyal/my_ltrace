#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/user.h>
#include <sys/reg.h>

#include "handle_elf.hh"

static char *
get_string(pid_t pid, void *addr);

static void
handle_strarg(char *func_name, struct user_regs_struct regs, pid_t pid);

static void
handle_intarg(char *func_name, struct user_regs_struct regs);

static void
handle_ptrarg(char *func_name, struct user_regs_struct regs);

size_t
display_function_call(pid_t pid, struct user_regs_struct regs, s_elf_info *e)
{
  void *addr = (void *)(regs.rip - 1);
  size_t func_plt_index =
    ((long)addr - (long)((char *)e->plt_addr + 0x10)) / 0x10;

  printf("%s(", get_func_name(e, func_plt_index));
  handle_strarg(get_func_name(e, func_plt_index), regs, pid);
  handle_intarg(get_func_name(e, func_plt_index), regs);
  handle_ptrarg(get_func_name(e, func_plt_index), regs);
  printf(")\n");

  return func_plt_index;
}

static void
handle_strarg(char *func_name, struct user_regs_struct regs, pid_t pid)
{
  if (!strcmp(func_name, "getenv")
      || !strcmp(func_name, "printf")
      || !strcmp(func_name, "strlen")
      || !strcmp(func_name, "opendir"))
  {
    char *buf = get_string(pid, (void *)regs.rdi);
    printf("\"%s\"", buf);
    free(buf);
  }
  if (!strcmp(func_name, "strcmp"))
  {
    char *s1 = get_string(pid, (void *)regs.rdi);
    char *s2 = get_string(pid, (void *)regs.rsi);
    printf("\"%s\", \"%s\"", s1, s2);
    free(s1);
    free(s2);
  }
}

static void
handle_intarg(char *func_name, struct user_regs_struct regs)
{
  if (!strcmp(func_name, "malloc"))
    printf("%ld", (long)regs.rdi);
}

static void
handle_ptrarg(char *func_name, struct user_regs_struct regs)
{
  if (!strcmp(func_name, "free"))
    printf("%p", (void *)regs.rdi);
}

static char *
get_string(pid_t pid, void *addr)
{
  size_t buf_size = 1024;
  char *buf = (char *)malloc(buf_size);
  long chr;
  for (int i = 0; ; i += sizeof (chr))
  {
    if (i + sizeof (chr) > buf_size)
    {
      buf_size *= 2;
      buf = (char *)realloc(buf, buf_size);
    }
    chr = ptrace(PTRACE_PEEKDATA, pid, (char *)addr + i, NULL);
    memcpy(buf + i, &chr, sizeof (chr));
    if (memchr(&chr, 0, sizeof (chr)) != NULL)
      break;
  }
  return buf;
}
