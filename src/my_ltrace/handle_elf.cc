#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <elf.h>
#include <string.h>

#include "handle_elf.hh"

static int
is_elf(Elf64_Ehdr *hdr);

static size_t elf_size = 0;

Elf64_Ehdr *
open_elf(char *filename)
{
  int fd = open(filename, O_RDONLY);
  if (fd == -1)
  {
    fprintf(stderr, "my_ltrace: Can't open %s\n", filename);
    return NULL;
  }

  struct stat st;
  fstat(fd, &st);
  elf_size = st.st_size;
  Elf64_Ehdr *elf =
    (Elf64_Ehdr *)mmap(0, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);

  if (elf && is_elf(elf))
    return elf;
  else
  {
    fprintf(stderr, "my_ltrace: Can't open ELF file \"%s\"", filename);
    munmap(elf, st.st_size);
    return NULL;
  }
}

void
close_elf(Elf64_Ehdr *elf)
{
  munmap(elf, elf_size);
}

s_elf_info *
get_elf_info(Elf64_Ehdr *elf)
{
  s_elf_info *e = (s_elf_info *)malloc(sizeof (s_elf_info));

  e->ehdr = elf;
  e->shdr = (Elf64_Shdr *)((char *)elf + elf->e_shoff);

  e->dynsym = NULL;
  e->rela = NULL;

  char *sections_strings = (char *)elf + e->shdr[elf->e_shstrndx].sh_offset;

  for (int i = 0; i < elf->e_shnum; i++)
  {
    if (e->shdr[i].sh_type == SHT_DYNSYM)
      e->dynsym = &e->shdr[i];
    if (e->shdr[i].sh_type == SHT_RELA)
      e->rela = &e->shdr[i];
    if (!strcmp(sections_strings + e->shdr[i].sh_name, ".got.plt"))
    {
      e->got_addr = (void *)e->shdr[i].sh_addr;
      e->got_size = (unsigned long)e->shdr[i].sh_size;
    }
    if (!strcmp(sections_strings + e->shdr[i].sh_name, ".plt"))
    {
      e->plt_addr = (void *)e->shdr[i].sh_addr;
      e->plt_size = (unsigned long)e->shdr[i].sh_size;
      e->plt_entries = (long *)malloc(e->plt_size * sizeof (long));
    }
  }

  return e;
}

void
free_elf_info(s_elf_info *e)
{
  free(e->plt_entries);
  free(e);
}

char *
get_func_name(s_elf_info *e, size_t index)
{
  Elf64_Rela *rela_entry = (Elf64_Rela *)
    ((char *)e->ehdr + e->rela->sh_offset + index * sizeof (Elf64_Rela));

  int offset = ELF64_R_SYM(rela_entry->r_info);
  char *strtable = (char *)
    ((char *)e->ehdr + e->shdr[e->dynsym->sh_link].sh_offset);
  Elf64_Sym *symbols = (Elf64_Sym *)((char *)e->ehdr + e->dynsym->sh_offset);
  symbols += offset;
  return strtable + symbols->st_name;
}

static int
is_elf(Elf64_Ehdr *hdr)
{
  if (hdr->e_ident[EI_MAG0] != ELFMAG0
      || hdr->e_ident[EI_MAG1] != ELFMAG1
      || hdr->e_ident[EI_MAG2] != ELFMAG2
      || hdr->e_ident[EI_MAG3] != ELFMAG3
      || hdr->e_ident[EI_CLASS] != ELFCLASS64)
    return 0;
  return 1;
}
