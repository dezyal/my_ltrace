#include <stdlib.h>
#include <stdio.h>
#include <elf.h>

#include "my_ltrace.hh"
#include "handle_elf.hh"

static void
enable_ld_bind_now(char **envp);

int
main(int argc, char **argv, char **envp)
{
  if (argc == 1)
  {
    fprintf(stderr, "my_ltrace: too few arguments\n");
    return 1;
  }

  Elf64_Ehdr *elf = open_elf(argv[argc - 1]);
  if (!elf)
    return 1;
  s_elf_info *e = get_elf_info(elf);
  enable_ld_bind_now(envp);

  trace_process(argc, argv, envp, e);

  close_elf(elf);
  free_elf_info(e);

  return 0;
}

static void
enable_ld_bind_now(char **envp)
{
  int i = 0;
  for (i = 0; envp[i]; i++)
    continue;
  envp[i - 1] = "LD_BIND_NOW=1";
  envp[i] = NULL;
}
