#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/user.h>
#include <sys/reg.h>
#include <errno.h>
#include <elf.h>

#include "my_ltrace.hh"
#include "display_func.hh"

static void
run_process(int argc, char **argv, char **envp);

static void
trace_pid(pid_t pid, s_elf_info *e, int display_address);

static void
set_breakpoints(s_elf_info *e, pid_t pid);

static void
restore_instruction(pid_t                    pid,
                    struct user_regs_struct  regs,
                    void                     *data,
                    void                     *addr);

static void
restore_breakpoint(pid_t pid, void *addr);

static void
list_functions(pid_t pid, s_elf_info *e);

void
trace_process(int argc, char **argv, char **envp, s_elf_info *e)
{
  int display_address = 0;
  if (argc > 2 && !strcmp(argv[1], "-i"))
    display_address = 1;

  int list_func = 0;
  if (argc > 2 && !strcmp(argv[1], "-l"))
    list_func = 1;

  pid_t pid = fork();
  if (!pid)
    run_process(argc, argv, envp);
  else
    !list_func ? trace_pid(pid, e, display_address)
              : list_functions(pid, e);
}

static void
run_process(int argc, char **argv, char **envp)
{
  ptrace(PTRACE_TRACEME, 0, NULL, NULL);
  if (execvpe(argv[argc - 1], argv + (argc - 1), envp) == -1)
    perror("error");
}

static void
trace_pid(pid_t pid, s_elf_info *e, int display_address)
{
  wait(0);

  set_breakpoints(e, pid);

  struct user_regs_struct regs;
  ptrace(PTRACE_CONT, pid, 0, 0);
  wait(0);

  while (1)
  {
    int status;
    ptrace(PTRACE_GETREGS, pid, 0, &regs);
    void *addr = (void *)(regs.rip - 1);

    if (display_address)
      printf("[%p] ", addr);
    size_t index = display_function_call(pid, regs, e);
    restore_instruction(pid, regs, (void *)e->plt_entries[index], addr);

    wait(&status);
    if (WIFEXITED(status))
      break;

    restore_breakpoint(pid, addr);

    ptrace(PTRACE_CONT, pid, 0, 0);
    wait(&status);
    if (WIFEXITED(status))
      break;
  }
}

static void
set_breakpoints(s_elf_info *e, pid_t pid)
{
  // Backup PLT entries
  for (size_t i = 0; i < (e->plt_size / 0x10 - 0x10); i++)
  {
    void *addr = (void *)((char *)e->plt_addr + i * 0x10 + 0x10);
    e->plt_entries[i] = ptrace(PTRACE_PEEKTEXT, pid, addr, 0);
  }
  // Set breakpoints
  long breakpoint = 0xCC;
  for (size_t i = 0; i < (e->plt_size / 0x10 - 0x10); i++)
  {
    void *addr = (void *)((char *)e->plt_addr + i * 0x10 + 0x10);
    ptrace(PTRACE_POKETEXT, pid, addr, (void*)breakpoint);
  }
}

static void
restore_instruction(pid_t                    pid,
                    struct user_regs_struct  regs,
                    void                     *data,
                    void                     *addr)
{
  regs.rip--;
  ptrace(PTRACE_SETREGS, pid, 0, &regs);
  ptrace(PTRACE_POKETEXT, pid, addr, data);
  ptrace(PTRACE_SINGLESTEP, pid, 0, 0);
}

static void
restore_breakpoint(pid_t pid, void *addr)
{
  long breakpoint = 0xCC;
  ptrace(PTRACE_POKETEXT, pid, addr, (void*)breakpoint);
}

static void
list_functions(pid_t pid, s_elf_info *e)
{
  int status;

  wait(&status);
  long ref = ptrace(PTRACE_PEEKDATA, pid, (char *)e->got_addr + 8 * 3, NULL);
  while (1)
  {
    ptrace(PTRACE_SYSCALL, pid, 0, 0);
    wait(&status);
    if (WIFEXITED(status))
      break;
    if (ref != ptrace(PTRACE_PEEKDATA, pid, (char *)e->got_addr + 8 * 3, NULL))
    {
      for (size_t i = 3 * 8; i < e->got_size; i += sizeof (Elf64_Addr))
      {
        long addr =
          ptrace(PTRACE_PEEKDATA, pid, (char *)e->got_addr + i, NULL);

        int func_got_index = i / sizeof (Elf64_Addr) - 3;
        printf("[%p] %s() = %p\n",
               (void *)((char *)e->got_addr + i),
               get_func_name(e, func_got_index),
               (void *)addr);
      }
      break;
    }
  }
}
