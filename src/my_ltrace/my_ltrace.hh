#ifndef MY_LTRACE_HH
# define MY_LTRACE_HH

# include "handle_elf.hh"

void
trace_process(int argc, char **argv, char **envp, s_elf_info *e);

#endif /* !MY_LTRACE_HH */
