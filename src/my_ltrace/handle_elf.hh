#ifndef HANDLE_ELF_HH
# define HANDLE_ELF_HH

# include <elf.h>

Elf64_Ehdr *
open_elf(char *filename);

void
close_elf(Elf64_Ehdr *elf);

typedef struct elf_info
{
  Elf64_Ehdr *ehdr;
  Elf64_Shdr *shdr;
  Elf64_Shdr *dynsym;
  Elf64_Shdr *rela;
  void *got_addr;
  unsigned long got_size;
  void *plt_addr;
  unsigned long plt_size;
  long *plt_entries;
} s_elf_info;

s_elf_info *
get_elf_info(Elf64_Ehdr *elf);

void
free_elf_info(s_elf_info *e);

char *
get_func_name(s_elf_info *e, size_t index);

#endif /* !HANDLE_ELF_HH */
